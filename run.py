from train import run_qg_qa

args_for_qa_dict = {
    "output_dir": "t5-small-qa",
    "train_file_path": "data/train_data_qa_qg_hl_t5.pt",
    "valid_file_path": "data/valid_data_qg_hl_t5.pt",
    "per_device_train_batch_size": 16,
    "per_device_eval_batch_size": 16,
    "gradient_accumulation_steps": 8,
    "learning_rate": 1e-4,
    "num_train_epochs": 3,
    "seed": 42,
    "do_train": True,
    "do_eval": True,
    "evaluate_during_training": True,
    "logging_steps": 100
}

args_for_qg_dict = {
    "output_dir": "multi_t5-qg",
    "train_file_path": "data/train_data_qg_hl_t5.pt",
    "valid_file_path": "data/valid_data_qg_hl_t5.pt",
    "per_device_train_batch_size": 16,
    "per_device_eval_batch_size": 16,
    "gradient_accumulation_steps": 4,
    "learning_rate": 1e-4,
    "num_train_epochs": 3,
    "seed": 27,
    "do_train": True,
    "do_eval": True,
    "evaluate_during_training": True,
    "logging_steps": 500
}

# start training
run_qg_qa(args_for_qg_dict)