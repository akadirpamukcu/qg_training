import dataclasses
import json
import logging
import os
import sys
from dataclasses import dataclass, field
from typing import Dict, List, Optional, Any, Union, Callable, Iterable
import numpy as np
import torch
from torch import nn

from transformers.file_utils import is_apex_available

if is_apex_available():
    from apex import amp

from transformers import (
    AutoModelForSeq2SeqLM,
    AutoTokenizer,
    T5Tokenizer,
    HfArgumentParser,
    DataCollator,
    TrainingArguments,
    set_seed,
    Trainer as HFTrainer,
)

from data_collator import T2TDataCollator

logger = logging.getLogger(__name__)

class Trainer(HFTrainer):
    def __init__(self, label_smoothing: float = 0, **kwargs):
        super().__init__(**kwargs)
        self.label_smoothing = label_smoothing
    
    def _training_step(self, model: nn.Module, inputs: Dict[str, Union[torch.Tensor, Any]], optimizer: torch.optim.Optimizer) -> float:
        model.train()
        for k, v in inputs.items():
            if isinstance(v, torch.Tensor):
                inputs[k] = v.to(self.args.device)
        if self.label_smoothing == 0:
            outputs = model(**inputs)
            loss = outputs[0]  # model outputs are always tuple in transformers (see doc)
        if self.args.n_gpu > 1:
            loss = loss.mean()  # mean() to average on multi-gpu parallel training
        loss = loss / self.args.gradient_accumulation_steps

        loss.backward()

        return loss.item()


@dataclass
class DataTrainingArguments:
    train_file_path: str 
    valid_file_path: str


def main(args_file=None):
    parser = HfArgumentParser((DataTrainingArguments, TrainingArguments))
    args_file_path = os.path.abspath(sys.argv[1]) if args_file is None else args_file
    data_args, training_args = parser.parse_json_file(json_file=args_file_path)   
    model_name = "google/mt5-small"
    #tokenizer_name = "t5_qg_tokenizer"
    cache_dir = None
    
    logging.basicConfig(
        format="%(asctime)s - %(levelname)s - %(name)s -   %(message)s",
        datefmt="%m/%d/%Y %H:%M:%S",
        level=logging.INFO if training_args.local_rank in [-1, 0] else logging.WARN,
    )
    logger.info("Training/evaluation parameters %s", training_args)

    set_seed(training_args.seed)
    tokenizer = AutoTokenizer.from_pretrained("google/mt5-small")
    #tokenizer_cls = T5Tokenizer
    #tokenizer = tokenizer_cls.from_pretrained(
    #    tokenizer_name if tokenizer_name else model_name,
    #    cache_dir=cache_dir,
    #)
    model = AutoModelForSeq2SeqLM.from_pretrained(
        model_name,
        cache_dir=cache_dir,
    )
    model.resize_token_embeddings(len(tokenizer))
    logger.info('loading dataset')
    train_dataset = torch.load(data_args.train_file_path) 
    valid_dataset = torch.load(data_args.valid_file_path) if training_args.do_eval else None
    data_collator = T2TDataCollator(
        tokenizer=tokenizer,
        mode="training",
    )
    # Initialize our Trainer
    trainer = Trainer(
        model=model,
        args=training_args,
        train_dataset=train_dataset,
        eval_dataset=valid_dataset,
        data_collator=data_collator,
        prediction_loss_only=True,
        label_smoothing=0
    )
    # Training
    if training_args.do_train:
        trainer.train(
            model_path=model_name if os.path.isdir(model_name) else None
        )
        trainer.save_model()
        if trainer.is_world_master():
            tokenizer.save_pretrained(training_args.output_dir)

    # Evaluation
    results = {}
    if training_args.do_eval and training_args.local_rank in [-1, 0]:
        logger.info("*** Evaluate ***")

        eval_output = trainer.evaluate()

        output_eval_file = os.path.join(training_args.output_dir, "eval_results.txt")
        with open(output_eval_file, "w") as writer:
            logger.info("***** Eval results *****")
            for key in sorted(eval_output.keys()):
                logger.info("  %s = %s", key, str(eval_output[key]))
                writer.write("%s = %s\n" % (key, str(eval_output[key])))
    
        results.update(eval_output)
    
    return results

def run_qg_qa(args_dict):
    with open("args.json", 'w') as f:
        json.dump(args_dict, f)
    main(args_file="args.json")

if __name__ == "__main__":
    main()